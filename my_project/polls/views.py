from django.shortcuts import get_object_or_404, redirect, render
from django.forms.forms import Form
from django.http import HttpResponse
from .forms import ChoiceForm, QuestionForm
from .models import Choice, Question
from datetime import datetime
from django.contrib import messages
from .models import Choice
from . models import Question
# Create your views here.
def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    choice = Choice.objects.all()
    context = {
        'latest_question_list':latest_question_list,
        'choice': choice,
    }
    return render(request, 'polls/index.html' , context)

def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    context = {
        'question' : question,
    }

    return render(request, 'polls/detail.html', context)

def detail_v2(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question':question})

def choiceDef(request, pk):
    choice = Choice.objects.get(pk=pk)
    context = {
        'choice':choice
    }
    return render(request, 'polls/choicedetail.html', context)

def questionForm(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            new_question = form.save(commit=False)
            new_question.pub_date = datetime.now()
            new_question.save()
            messages.info(request, 'You have successfully added the question.')
            return redirect('question_form')
    else:
        form = QuestionForm()
        context = {
            'form':form,
        }
        return render(request, 'polls/addquestion.html', context)

def choiceForm(request):
    if request.method == 'POST':
        form = ChoiceForm(request.POST)
        if form.is_valid():
            new_choice = form.save()
            messages.info(request, 'you have successfully added a choice.')
            return redirect('choice_form')

    else:
        form = ChoiceForm()
    context = {
        'form':form,
    }
    return render(request, 'polls/addchoice.html', context)

def updateQuestion(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    form = QuestionForm(instance = question)
    if request.method == 'POST':
        form = QuestionForm(request.POST, instance=question)
        if form.is_valid():
            form.save()
            return redirect('index')
    context = {
        'form':form,
    }
    return render(request, 'polls/editdetail.html', context)

def deleteQuestion(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    question.delete()
    return redirect('index')

